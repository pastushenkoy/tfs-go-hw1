package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strconv"
	"time"
)

type dimentions struct {
	begin  time.Time
	ticker string
}

type ohlc struct {
	o                 float64
	h                 float64
	l                 float64
	c                 float64
	earliestTradeTime time.Time
	latestTradeTime   time.Time
}

type point struct {
	ticker    string
	timestamp time.Time
	price     float64
}

type candleStickChart map[dimentions]ohlc

func main() {

	csvFile, _ := os.Open("trades.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))

	chart5min := make(candleStickChart)
	chart30min := make(candleStickChart)
	chart240min := make(candleStickChart)

	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}

		if len(line) != 4 {
			continue
		}

		point := parsePoint(line)
		hour := point.timestamp.Hour()
		if hour >= 3 && hour < 7 {
			continue
		}

		chart5min = appendPoint(chart5min, point, func(timestamp time.Time) time.Time { return timestamp.Truncate(5 * time.Minute) })
		chart30min = appendPoint(chart30min, point, func(timestamp time.Time) time.Time { return timestamp.Truncate(30 * time.Minute) })
		chart240min = appendPoint(chart240min, point, func(timestamp time.Time) time.Time {
			return timestamp.Add(-180 * time.Minute).Truncate(240 * time.Minute).Add(180 * time.Minute) //
		})
	}

	writeChartToDisk(chart5min, "candles_5min.csv")
	writeChartToDisk(chart30min, "candles_30min.csv")
	writeChartToDisk(chart240min, "candles_240min.csv")
}

func parsePoint(line []string) point {
	ticker := line[0]
	price, _ := strconv.ParseFloat(line[1], 64)
	timestamp, _ := time.Parse("2006-01-02 15:04:05.999999", line[3])

	return point{
		ticker:    ticker,
		timestamp: timestamp,
		price:     price,
	}
}

func appendPoint(chart candleStickChart, pnt point, intervalEvaluator func(timestamp time.Time) time.Time) candleStickChart {
	intervalBegin := intervalEvaluator(pnt.timestamp)

	dim := dimentions{
		begin:  intervalBegin,
		ticker: pnt.ticker,
	}

	if interval, ok := chart[dim]; ok {

		if interval.l > pnt.price {
			interval.l = pnt.price
		}
		if interval.h < pnt.price {
			interval.h = pnt.price
		}
		if interval.earliestTradeTime.After(pnt.timestamp) {
			interval.o = pnt.price
			interval.earliestTradeTime = pnt.timestamp
		}
		if interval.latestTradeTime.Before(pnt.timestamp) || interval.latestTradeTime.Equal(pnt.timestamp) {
			interval.c = pnt.price
			interval.latestTradeTime = pnt.timestamp
		}

		chart[dim] = interval
	} else {
		interval := ohlc{
			o:                 pnt.price,
			l:                 pnt.price,
			h:                 pnt.price,
			c:                 pnt.price,
			earliestTradeTime: pnt.timestamp,
			latestTradeTime:   pnt.timestamp,
		}
		chart[dim] = interval
	}

	return chart
}

func writeChartToDisk(chart candleStickChart, filename string) {
	outputDirName := "output"
	if _, err := os.Stat(outputDirName); os.IsNotExist(err) {
		os.Mkdir(outputDirName, os.ModePerm)
	}

	file, err := os.Create(path.Join("output", filename))
	if err != nil {
		panic(err)
	}
	writer := bufio.NewWriter(file)
	defer writer.Flush()

	for dim, value := range chart {
		dateStr := dim.begin.Format("2006-01-02T15:04:05Z")
		writer.WriteString(fmt.Sprintf("%s,%s,%.2f,%.2f,%.2f,%.2f\n", dim.ticker, dateStr, value.o, value.h, value.l, value.c))
	}
}
